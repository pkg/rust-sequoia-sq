rust-sequoia-sq (1.2.0-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 16:31:50 +0000

rust-sequoia-sq (1.2.0-1) unstable; urgency=medium

  * Package sequoia-sq 1.2.0 from crates.io using debcargo 2.7.6

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Wed, 05 Feb 2025 12:08:59 +0100

rust-sequoia-sq (0.40.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.40.0 from crates.io using debcargo 2.7.5
  * Bump terminal-size to v0.4 (Closes: #1092378)

 -- NoisyCoil <noisycoil@tutanota.com>  Tue, 07 Jan 2025 23:37:47 +0000

rust-sequoia-sq (0.40.0-1) unstable; urgency=medium

  * Package sequoia-sq 0.40.0 from crates.io using debcargo 2.7.4
    - refresh patches.
    - drop man-pages dropped upstream, simplify debian/sq.manpages.

 -- Holger Levsen <holger@debian.org>  Fri, 29 Nov 2024 17:29:35 +0100

rust-sequoia-sq (0.38.0-3) unstable; urgency=medium

  * Package sequoia-sq 0.38.0 from crates.io using debcargo 2.7.0
    - ship upstream manpages and shell completions, generated at build time.

 -- Holger Levsen <holger@debian.org>  Sun, 13 Oct 2024 18:40:36 +0200

rust-sequoia-sq (0.38.0-2) unstable; urgency=medium

  * Package sequoia-sq 0.38.0 from crates.io using debcargo 2.7.0
    - relax dependencies to allow build with -policy-config 0.7.

 -- Holger Levsen <holger@debian.org>  Sat, 12 Oct 2024 13:38:34 +0200

rust-sequoia-sq (0.38.0-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.38.0 from crates.io using debcargo 2.7.0 (Closes: #1081757)
  * Update patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 24 Sep 2024 08:39:42 +0000

rust-sequoia-sq (0.37.0-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.37.0 from crates.io using debcargo 2.6.1 (Closes: #1074679)
  * Bump sequoia-openpgp dependency to pick up the fix for RUSTSEC-2024-0345

  [ Holger Levsen ]
  * Package sequoia-sq 0.37.0 from crates.io using debcargo 2.6.1.
    Closes: #1073408.
  * Add Alexander and myself to uploaders.

  [ Alexander Kjäll ]
  * Package sequoia-sq 0.34.0 from crates.io using debcargo 2.6.1
  * Add man pages and bash completion to sequoia-sq. Closes: #1061665.
    Closes: #1062705.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 23 Jul 2024 13:47:04 +0000

rust-sequoia-sq (0.33.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.33.0 from crates.io using debcargo 2.6.1
  * Relax itertools dependency.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 27 Jan 2024 18:26:29 +0000

rust-sequoia-sq (0.33.0-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.33.0 from crates.io using debcargo 2.6.1
  * Update copyright file for upstream license change.
  * Fix installation of shell completion files with new upstream version.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 25 Jan 2024 13:50:54 +0000

rust-sequoia-sq (0.31.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.31.0 from crates.io using debcargo 2.6.1
  * Stop patching rpassword dependency.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 27 Nov 2023 22:46:50 +0000

rust-sequoia-sq (0.31.0-1) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.31.0 from crates.io using debcargo 2.6.0 (Closes: #1040959, #1042209)
  * Update drop-subplot.patch for new upstream.
  * Drop relax-serde_json.patch and rpassword-6.patch, no longer needed.
  * Revert upstream bump of rpassword from 6 to 7.
  * Drop reference to manpage that no longer seems to exist.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Nov 2023 21:58:44 +0000

rust-sequoia-sq (0.27.0-2+apertis2) apertis; urgency=medium

  * Bump changelog to trigger license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 27 Jul 2023 14:36:57 +0530

rust-sequoia-sq (0.27.0-2+apertis1) apertis; urgency=medium

  * Sync updates from debian/bookworm

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Mon, 01 May 2023 14:48:11 +0530

rust-sequoia-sq (0.27.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.27.0 from crates.io using debcargo 2.6.0
  * Apply upstream patch for rpassword 6.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 09 Feb 2023 04:32:50 +0000

rust-sequoia-sq (0.27.0-1) unstable; urgency=medium

  * Package sequoia-sq 0.27.0 from crates.io using debcargo 2.5.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 20 Jul 2022 01:12:11 -0400

rust-sequoia-sq (0.26.0-3) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.26.0 from crates.io using debcargo 2.5.0
  * Bump dependency on sequoia-net to 0.25 (Closes: #1015109)

 -- Peter Michael Green <plugwash@debian.org>  Sun, 17 Jul 2022 01:55:48 +0000

rust-sequoia-sq (0.26.0-2) unstable; urgency=medium

  * Package sequoia-sq 0.26.0 from crates.io using debcargo 2.5.0
  * Ship correct manpages now that the net feature is enabled.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 08 Jul 2022 15:04:23 -0400

rust-sequoia-sq (0.26.0-1) unstable; urgency=medium

  * Package sequoia-sq 0.26.0 from crates.io using debcargo 2.5.0
  * drop subplot tests since subplot is not in debian

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 08 Jul 2022 02:36:56 -0400

rust-sequoia-sq (0.25.0-3) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.25.0 from crates.io using debcargo 2.5.0
  * Relax dependency on sequoia-autocrypt.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 Dec 2021 13:03:46 +0000

rust-sequoia-sq (0.25.0-2) unstable; urgency=medium

  * Team upload.
  * Package sequoia-sq 0.25.0 from crates.io using debcargo 2.4.4

 -- Peter Michael Green <plugwash@debian.org>  Sun, 24 Oct 2021 17:14:53 +0000

rust-sequoia-sq (0.25.0-1) unstable; urgency=medium

  * Package sequoia-sq 0.25.0 from crates.io using debcargo 2.4.4

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 05 Mar 2021 19:34:07 -0500

rust-sequoia-sq (0.24.0-4+apertis2) apertis; urgency=medium

  * Add a patch to relax dependency on rust-itertools,
     to allow building it against rust-itertools 0.10.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Wed, 09 Feb 2022 22:44:26 +0100

rust-sequoia-sq (0.24.0-4+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Wed, 06 Oct 2021 12:56:59 +0530

rust-sequoia-sq (0.24.0-4+apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 06 Jul 2021 07:01:34 +0000

rust-sequoia-sq (0.24.0-4) unstable; urgency=medium

  * Package sequoia-sq 0.24.0 from crates.io using debcargo 2.4.3
  * Enable autocrypt functionality

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 04 Feb 2021 08:48:17 -0500

rust-sequoia-sq (0.24.0-3) unstable; urgency=medium

  * Package sequoia-sq 0.24.0 from crates.io using debcargo 2.4.3
  * indicate Breaks/Replaces: ispell (<< 3.4.00-6.1~) (Closes: #981579)
    Thanks, Andreas Beckmann!

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 01 Feb 2021 20:23:35 -0500

rust-sequoia-sq (0.24.0-2) unstable; urgency=medium

  * Package sequoia-sq 0.24.0 from crates.io using debcargo 2.4.3
  * No-op source-only re-upload for Debian Testing Migration.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 30 Jan 2021 23:05:22 -0500

rust-sequoia-sq (0.24.0-1) unstable; urgency=medium

  * Package sequoia-sq 0.24.0 from crates.io using debcargo 2.4.3
    (Closes: #929385)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 28 Jan 2021 17:27:10 -0500
